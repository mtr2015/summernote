<?php
// // A list of permitted file extensions
// $allowed = array('png', 'jpg', 'gif','zip');

// if(isset($_FILES['file']) && $_FILES['file']['error'] == 0){

//     $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

//     if(!in_array(strtolower($extension), $allowed)){
//         echo '{"status":"error for extension"}';
//         exit;
//     }

//     if(move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/'.$_FILES['file']['name'])){
//         $tmp='uploads/'.$_FILES['file']['name'];
//         echo 'uploads/'.$_FILES['file']['name'];
//         //echo '{"status":"success"}';
//         exit;
//     }
// }

// echo '{"status":"error for upload"}';
// exit;

$error = ""; //上传文件出错信息
$msg = "";
$fileElementName = 'file';
$allowType = array(".jpg",".gif",".png"); //允许上传的文件类型
$num      = strrpos($_FILES['file']['name'] ,'.');  
$fileSuffixName    = substr($_FILES['file']['name'],$num,8);//此数可变  
$fileSuffixName    = strtolower($fileSuffixName); //确定上传文件的类型

$upFilePath             = './uploads/'; //最终存放路径

if(!empty($_FILES[$fileElementName]['error']))
{
   switch($_FILES[$fileElementName]['error'])
   {
    case '1':
     $error = '传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值';
     break;
    case '2':
     $error = '上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值';
     break;
    case '3':
     $error = '文件只有部分被上传';
     break;
    case '4':
     $error = '没有文件被上传';
     break;
    case '6':
     $error = '找不到临时文件夹';
     break;
    case '7':
     $error = '文件写入失败';
     break;
    default:
     $error = '未知错误';
   }
}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
{
   $error = '没有上传文件.';
}elseif(!in_array($fileSuffixName,$allowType))
{
   $error = '不允许上传的文件类型'; 
}elseif($ok === FALSE){
    $error = '上传失败';
}

?>