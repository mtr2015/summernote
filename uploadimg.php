<?php
function upload_img()
    {
        if ($_FILES) {
            if (!$_FILES['file']['error']) {
                //生成的文件名（时间戳，精确到毫秒）
                list($usec, $sec) = explode(" ", microtime());
                $name = ((float)$usec + (float)$sec) * 1000;

                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $folder = date("Ymd");
                $targetDir = C('UPLOAD_PICTURE_URL') . $folder;

                //如果上传的文件夹不存在，则创建之
                if ($targetDir) {
                    @mkdir($targetDir);
                }

                //文件目录
                $targetDir_url = $targetDir . '/article';

                //如果上传的文件夹不存在，则创建之
                if ($targetDir_url) {
                    @mkdir($targetDir_url);
                }

               //图片上传的具体路径就出来了
                $destination = $targetDir_url . DIRECTORY_SEPARATOR . $filename; //change this directory
                $location = $_FILES["file"]["tmp_name"];

                //将图片移动到指定的文件夹****核心代码
                move_uploaded_file($location, $destination);
                echo C('UPLOAD_PICTURE') . $folder . '/article' . DIRECTORY_SEPARATOR . $filename;//change this URL
            } else {
                echo $message = 'Ooops!  Your upload triggered the following error:  ' . $_FILES['file']['error'];
            }
        }
    }